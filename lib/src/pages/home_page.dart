import 'package:flutter/material.dart';
import 'package:componentes/src/providers/menu_provider.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes'),
      ),
      body: _lista(),
    );
  }

  Widget _lista() {
    return FutureBuilder(
      future: menuProvider.cargarData(),
      initialData: ['Cargando opciones'],
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        return ListView(
          children: _crearListaItems(snapshot.data),
        );
      },
    );
  }

  List<Widget> _crearListaItems(List<dynamic> data) {
    final List<Widget> opciones = [];
    data.forEach((opt) {
      final Widget tmp = ListTile(
        title: Text(opt['texto']),
        leading: Icon(Icons.account_circle, color: Colors.blueAccent),
        trailing: Icon(Icons.keyboard_arrow_right),
        onTap: () {},
      );
      opciones..add(tmp)
              ..add(Divider());
    });

    return opciones;
  }
}
